# inception-of-things

## Getting started

k3s documentation https://k3s.io/  
A github example https://github.com/paulbouwer/hello-kubernetes  
k3d documentation https://k3d.io/v5.1.0/usage/exposing_services  
A general project https://www.digitalocean.com/community/tutorials/how-to-set-up-an-nginx-ingress-on-digitalocean-kubernetes-using-helm

## p1

Vagrant will help you create two vms.  
The Vagrantfile will provide the config to set in each vms (cpus, name, etc.), and the box you choose will be the OS to install on them.  
You will then provision them with a bash script.  
This bash script will install k3s on the server machine and k3s-agent on the agent machine.  
So, well, you need a bash script for each.
The agent can join the server thanks to the `token`, which must be the same for both the server and the agent.  

Use the command line tool `kubectl` to check on your nodes.  
It is installed together with k3s - no need to worry about it!

## p2

In p2, you will learn how to create pods.  
You will get acquainted with the concepts of [pods](https://kubernetes.io/docs/concepts/workloads/pods/), [deployments](https://kubernetes.io/docs/concepts/workloads/controllers/deployment/) and [services](https://kubernetes.io/docs/concepts/services-networking/service/).  
Basically you hardly ever need to worry about pods, because they get created with deployments.  
Services are needed to expose your deployments outside your cluster (to the whole world and beyond!).  
And then you'll [ingress](https://kubernetes.io/docs/concepts/services-networking/service/) your way to what you have deployed.  
You can deploy each deployment and service by kubectl command line, or else make your own amazing-yaml-file.yaml and then `kubectl apply -f` the hell out of it.  

Do not forget to add `192.168.56.110 app1.com` etcetera to your `/etc/hosts` file of your physical machine.

## Things I wish I had known before

I did this project on a virtual machine and ... it took ages to compile.  I thought it was normal but it is not.  
I also thought my script was not working because the node would never join the cluster.    
But actually ... it joined (eventually), after about 50 minutes.   

<img src="image.png" width="75%" style="display: block; margin: 0 auto"/>

Based on your computer, it might be ok to use 2 cpus instead of 1 or 1024 memory instead of 512.   
If everything looks good but your node still can't join the cluster, check on that too.

## Errors and Fixes

:imp: ERROR:  
`Vagrant destroy -f` does not work.  
Machines are there but not created and cannot be upped.  
:innocent: FIX:
```
// probably better to just jump directly to vboxmanage
vagrant destroy
rm -rf .vagrant
vagrant global-status --prune
vboxmanage list vms   
vboxmanage unregistervm <id> --delete
```

:imp: ERROR:  
`/opt/vagrant/embedded/gems/gems/i18n-1.14.1/lib/i18n.rb:210:in translate: wrong number of arguments (given 2, expected 0..1) (ArgumentError)
from /home/creepconverter/.vagrant.d/gems/3.1.4/gems/vagrant-vbguest-0.21.0/lib/vagrant-vbguest/middleware.rb:24:in call`  
:innocent: FIX:  https://github.com/hashicorp/vagrant/issues/13232

:imp: ERROR:  
`Failed to apply container_runtime_exec_t to /usr/local/bin/k3s`    
:innocent: FIX:  https://github.com/k3s-io/k3s/issues/2162  

:imp: ERROR:  
`Waiting to retrieve agent configuration; server is not ready: Node password rejected`  
:innocent: FIX:  https://github.com/k3s-io/k3s/issues/4839#issuecomment-1001451351
