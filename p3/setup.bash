#!/usr/bin/env bash

# This script needs to be run as root on a brand new debian machine

if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit
fi

set -e
set -x

# installing deps
apt-get update
apt-get install -y curl

# docker (from docker docs)
curl -sSLf https://get.docker.com/ | bash

# kubectl (from kubernetes docs) and k3d
curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
curl -LO "https://dl.k8s.io/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl.sha256"
wget -q -O - https://raw.githubusercontent.com/k3d-io/k3d/main/install.sh | bash
echo "$(cat kubectl.sha256)  kubectl" | sha256sum --check
install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl

# setting up argocd
k3d cluster create mycluster --wait
kubectl create namespace argocd

# installing argocd in k3s cluster
kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml

