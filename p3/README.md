vogosphere: git@vogsphere-v2.s19.be:vogsphere/intra-uuid-3105a56d-cd11-4ba5-b39f-b59ac0dd8eea-4911959-mlazzare
gitlab: https://gitlab.com/BeBel42/inception-of-things
github: https://github.com/BeBel42/iot-argocd-mlefevre

sudo bash ./setup.bash # installs all the dependencies

sudo kubectl apply -f application.yaml # launches will's app
sudo kubectl port-forward service/argocd-server -n argocd 8080:443
sudo ./get-default-pass.bash # outputs the password for argo login
firefox localhost:8080 # to go to argocd ui (name is admin)
sudo kubectl port-forward service/wil-playground -n dev 8888:8888
curl localhost:8888 # to interact with wil's app

localhost:8080 -> argocd
localhost:8888 -> wil's app
Use Ubuntu 24.04 for this

Forwarding ports in the background does not work well - I tried
If something does not work, wait then reforward port (it needs some time)
Port forwarding will fail for 8888 when updating version - do it again
